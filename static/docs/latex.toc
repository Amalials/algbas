\babel@toc {spanish}{}
\contentsline {chapter}{\numberline {1}Conjuntos}{5}{chapter.1}%
\contentsline {section}{\numberline {1.1}Construcciones básicas}{5}{section.1.1}%
\contentsline {section}{\numberline {1.2}Aplicaciones}{18}{section.1.2}%
\contentsline {section}{\numberline {1.3}Conjuntos cociente}{30}{section.1.3}%
\contentsline {section}{\numberline {1.4}Factorización canónica de una aplicación}{34}{section.1.4}%
\contentsline {chapter}{\numberline {2}Grupos}{37}{chapter.2}%
\contentsline {section}{\numberline {2.1}Definiciones básicas}{37}{section.2.1}%
\contentsline {section}{\numberline {2.2}El grupo simétrico}{43}{section.2.2}%
\contentsline {section}{\numberline {2.3}Ciclos y trasposiciones}{46}{section.2.3}%
\contentsline {section}{\numberline {2.4}El signo de una permutación}{53}{section.2.4}%
\contentsline {section}{\numberline {2.5}Subgrupos}{57}{section.2.5}%
\contentsline {section}{\numberline {2.6}El teorema de Lagrange}{60}{section.2.6}%
\contentsline {section}{\numberline {2.7}Homomorfismos}{63}{section.2.7}%
\contentsline {section}{\numberline {2.8}Grupos cociente}{69}{section.2.8}%
\contentsline {chapter}{\numberline {3}Enteros}{77}{chapter.3}%
\contentsline {section}{\numberline {3.1}Anillos}{77}{section.3.1}%
\contentsline {section}{\numberline {3.2}Homomorfismos}{82}{section.3.2}%
\contentsline {section}{\numberline {3.3}Ideales}{84}{section.3.3}%
\contentsline {section}{\numberline {3.4}Cocientes}{86}{section.3.4}%
\contentsline {section}{\numberline {3.5}Dominios}{89}{section.3.5}%
\contentsline {section}{\numberline {3.6}Ideales primos}{91}{section.3.6}%
\contentsline {section}{\numberline {3.7}Divisibilidad en \(\symbb {Z}\)}{92}{section.3.7}%
\contentsline {section}{\numberline {3.8}Divisor común máximo}{96}{section.3.8}%
\contentsline {section}{\numberline {3.9}Primos}{102}{section.3.9}%
\contentsline {section}{\numberline {3.10}Congruencias}{105}{section.3.10}%
\contentsline {chapter}{\numberline {4}Polinomios}{111}{chapter.4}%
\contentsline {section}{\numberline {4.1}Anillos de polinomios}{111}{section.4.1}%
\contentsline {section}{\numberline {4.2}Irreducibles}{118}{section.4.2}%
\contentsline {section}{\numberline {4.3}Coeficientes complejos y reales}{119}{section.4.3}%
\contentsline {section}{\numberline {4.4}Coeficientes enteros y racionales}{121}{section.4.4}%
